class yubikey (
    Array[String] $otp_servers,
    String $credential_path,
    String $realm,
) {
    package {'pam_otp':
        ensure => 'installed',
    }

    file {'/etc/pam_otp.cred':
        ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0400',
    }

    file {'/etc/pam.d/sshd':
        ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('yubikey/pam_sshd.conf.epp', {
	    'otp_servers' => $otp_servers,
	    'realm' => $realm,
	    'credential_path' => $credential_path,
	}),
    }
}
